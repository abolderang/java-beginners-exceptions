# Project Name

Exception exercise project. Use best practices to write Exception 

## Installation

Gebruik bij voorkeur de Spring Tool Suite 3.6.4 of later. 


## Usage

### Stap 1 invoer validatie
In deze stap gaan we de methode importeerBestand in de FileImporterServiceImpl uitbreiden.  

Volg het fail fast principe en doe eerst een invoer validatie. De bestandsnaam mag niet leeg zijn en niet null. Gebruik hiervoor een bestaande Exceptie een IllegalArgumentException. Omdat de client/gebruiker van deze service methode een herstelactie zou kunnen uitvoeren o.b.v. deze invoer gaan we de Exceptie opnemen als throws in de methode declaratie.  

### Stap 2 Implementeer de printOutputMethode en Log de inhoud van het bestand als dummy implementatie van het importeren.

Maak de print output verder af en gebruik hiervoor de onderstaande code. Nu gaat het erom dat je een try, catch, finally goed gebruikt. Let erop dat je in een finally geen nieuwe exceptie gooit, vang deze af en log de output. Wanneer het bestand niet bestaat wordt er een FileNotFound Exceptie gegooid en deze wil je als gebruiker ook weten, dus gooi deze door tot aan de method declaratie van de importeerBestand methode.  

	BufferedReader br = new BufferedReader(new FileReader(importBestand));
	String line;
	while ((line = br.readLine()) != null) {
		LOGGER.debug(line);
	}
	br.close();
		

## History

TODO: Write history

## Credits

TODO: Write credits

## License
 
 TODO: Write License