package nl.angarde.javabeg.exception.service;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Test;

import nl.angarde.javabeg.exception.service.FileImporterServiceImpl;

public class FileImporterServiceExceptionTest {

	FileImporterServiceImpl fileImporterServiceImpl = new FileImporterServiceImpl();

	@Test(expected = IllegalArgumentException.class)
	public void testImporteerBestand_GeenbestandsnaamOpgegeven()
			throws IllegalArgumentException, FileNotFoundException {
		fileImporterServiceImpl.importeerBestand(null);

	}

	@Test(expected = FileNotFoundException.class)

	public void testImporteerBestand_InputOutputError() throws IllegalArgumentException, FileNotFoundException {
		fileImporterServiceImpl.importeerBestand("bestand_bestaat_niet");

	}

	@Test
	public void testImporteerBestand_Success() throws IllegalArgumentException, FileNotFoundException, URISyntaxException {
		
		URL url = this.getClass().getClassLoader().getResource("print_mij.txt");
		String bestandsNaam = url.toURI().toString().replace("file:/", "");
		
		fileImporterServiceImpl.importeerBestand(bestandsNaam);

	}

}
