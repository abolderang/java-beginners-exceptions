package nl.angarde.javabeg.exception.service;

import java.io.File;

/**
 * De FileImporterServiceImpl importeert fictief een bestand van een verkeerde
 * lokatie om een Exception te krijgen. Het doel van deze service is het maken
 * van een goede foutafhandeling.
 * 
 * @author Andre
 *
 */
public class FileImporterServiceImpl {

	/**
	 * Importeer een bestand met gegeven bestandsnaam.
	 * 
	 * @param bestandsNaam
	 *            Naam van het bestand dat ge�mporteerd moet worden.
	 * 
	 */
	public void importeerBestand(String bestandsNaam) {

		File importBestand = new File(bestandsNaam);

		printOutput(importBestand);

	}

	private void printOutput(File importBestand) {
		// TODO Auto-generated method stub

	}

}
